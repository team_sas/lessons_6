package geekbrains.java_1.lesson_6;

import geekbrains.java_1.lesson_6.animals.Animal;
import geekbrains.java_1.lesson_6.animals.cats.Cat;
import geekbrains.java_1.lesson_6.animals.dogs.Dog;

public class MainClass {

    /**
     * Задание 1
     * Создать классы Собака и Кот с наследованием от класса Животное.
     *
     * Задание 2
     * Животные могут выполнять действия: бежать, плыть, перепрыгивать препятствие. В качестве параметра каждому
     * методу передается величина, означающая или длину препятствия (для бега и плавания), или высоту (для прыжков).
     *
     * Задание 3
     * У каждого животного есть ограничения на действия (бег: кот 200 м., собака 500 м.; прыжок: кот 2 м.,
     * собака 0.5 м.; плавание: кот не умеет плавать, собака 10 м.).
     *
     * Задание 4
     * При попытке животного выполнить одно из этих действий, оно должно сообщить результат в консоль.
     * Например, dog1.run(150); -> результат: run: true.
     *
     * Задание 5
     * Добавить животным разброс в ограничениях. То есть у одной собаки ограничение на бег может быть 400 м.,
     * у другой 600 м.
     *
     * @param args
     */
    public static void main(String[] args) {
        Animal[] animals = new Animal[6];

        for(int i = 0; i < animals.length; i++) {
            if (i % 2 == 0)
            animals[i] = new Cat("Кот #" + i);
            else animals[i] = new Dog("Собака #" + i);
        }

        for (int i = 0; i < animals.length; i++) {
            animals[i].printInfo();
            animals[i].run(150);
            animals[i].swim(4);
            animals[i].jump(1);
        }
    }
}
