package geekbrains.java_1.lesson_6.animals;

public abstract class Animal {

    protected String name;
    protected int runDistance;
    protected int swimDistance;
    protected float jumpHeight;

    public Animal(String name, int runDistance, int swimDistance, float jumpHeight) {
        this.name = name;
        this.runDistance = runDistance;
        this.swimDistance = swimDistance;
        this.jumpHeight = jumpHeight;
    }

    public void run(int width) {
        boolean canRun = runDistance >= width;
        System.out.println(name + " run(" + width + "): " + canRun);
    }

    public void swim(int width) {
        boolean canSwim = swimDistance >= width;
        System.out.println(name + " swim(" + width + "): " + canSwim);
    }

    public void jump(int height) {
        boolean canJump = swimDistance >= height;
        System.out.println(name + " jump(" + height + "): " + canJump);
    }

    public void printInfo() {
        System.out.println(
            name + ": runDistance - " +
            runDistance + ", swimDistance - " +
            swimDistance + ", jumpHeight - " +
            jumpHeight + "."
        );
    }
}
