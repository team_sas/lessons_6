package geekbrains.java_1.lesson_6.animals.cats;

import geekbrains.java_1.lesson_6.animals.Animal;

public class Cat extends Animal {

    public static final int MIN_RUN_DISTANCE = 10;
    public static final int MAX_RUN_DISTANCE = 200;

    public static final float MIN_JUMP_HEIGHT = 0.5f;
    public static final float MAX_JUMP_HEIGHT = 2f;

    public Cat(String name) {
        super(
            name,
            (MIN_RUN_DISTANCE + (int) (Math.random() * ((MAX_RUN_DISTANCE - MIN_RUN_DISTANCE) + 1))),
            0,
            (MIN_JUMP_HEIGHT + (float) (Math.random() * (MAX_JUMP_HEIGHT - MIN_JUMP_HEIGHT)))
        );
    }

    @Override
    public void swim(int width) {
        System.out.println(name + " не умеет плавать.");
    }
}
