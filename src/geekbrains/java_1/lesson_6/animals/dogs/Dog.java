package geekbrains.java_1.lesson_6.animals.dogs;

import geekbrains.java_1.lesson_6.animals.Animal;

public class Dog extends Animal {

    public static final int MIN_RUN_DISTANCE = 10;
    public static final int MAX_RUN_DISTANCE = 500;

    public static final int MIN_SWIM_DISTANCE = 1;
    public static final int MAX_SWIM_DISTANCE = 10;

    public static final float MIN_JUMP_HEIGHT = 0.1f;
    public static final float MAX_JUMP_HEIGHT = 0.5f;

    public Dog(String name) {
        super(
            name,
            (MIN_RUN_DISTANCE + (int) (Math.random() * ((MAX_RUN_DISTANCE - MIN_RUN_DISTANCE) + 1))),
            (MIN_SWIM_DISTANCE + (int) (Math.random() * ((MAX_SWIM_DISTANCE - MIN_SWIM_DISTANCE) + 1))),
            (MIN_JUMP_HEIGHT + (float) (Math.random() * (MAX_JUMP_HEIGHT - MIN_JUMP_HEIGHT)))
        );
    }
}
